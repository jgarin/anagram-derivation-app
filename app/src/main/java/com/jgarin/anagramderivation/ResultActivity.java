package com.jgarin.anagramderivation;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

	public static final String ORIGINAL_WORD = "originalWord";
	public static final String DERIVATIONS = "derivations";

	public static void launch(final Context context, final String originalWord, final ArrayList<String> derivations) {
		Intent intent = new Intent(context, ResultActivity.class);
		intent.putExtra(ORIGINAL_WORD, originalWord);
		intent.putStringArrayListExtra(DERIVATIONS, derivations);
		context.startActivity(intent);
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);

		String originalWord = getIntent().getStringExtra(ORIGINAL_WORD);
		setTitle(getString(R.string.result_for_xxx, originalWord));

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().replace(R.id.fragment_container, new ResultFragment()).commit();
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
}
