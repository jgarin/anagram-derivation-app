package com.jgarin.anagramderivation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FilePickerActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_picker);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().replace(R.id.fragment_container, new FilePickerFragment()).commit();
		}
	}
}
