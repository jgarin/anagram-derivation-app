package com.jgarin.anagramderivation;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Arrays;

import static android.app.Activity.RESULT_OK;

/**
 * Created by jgarin on 16/01/2017.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

	private static final int REQUEST_CODE = 123;
	private static final String[] predefinedDictionary = new String[]{"ail", "tennis", "nails", "desk", "aliens", "table", "engine", "sail"};
	private ProgressBar progressBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true); // to survive rotation
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_hone, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
		view.findViewById(R.id.btn_from_file).setOnClickListener(this);
		view.findViewById(R.id.btn_predefined).setOnClickListener(this);
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_predefined:
				WordInputActivity.launch(getActivity(), new ArrayList<>(Arrays.asList(predefinedDictionary)));
				break;
			case R.id.btn_from_file:
				startActivityForResult(new Intent(getActivity(), FilePickerActivity.class), REQUEST_CODE);
				break;
			default:
				break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
			Uri fileUri = data.getData();
			progressBar.setVisibility(View.VISIBLE);
			new FileReaderTask(getActivity().getContentResolver()) {
				@Override
				protected void onPostExecute(ArrayList<String> dictionary) {
					if (dictionary != null) {
						WordInputActivity.launch(getActivity(), dictionary);
					}
				}
			}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, fileUri);
		}
	}


}
