package com.jgarin.anagramderivation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by jgarin on 17/01/2017.
 */
public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ViewHolder> {

	private final ArrayList<String> derivations;

	public ResultAdapter(final ArrayList<String> derivations) {
		this.derivations = derivations;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.result_list_item, parent, false));
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.onBind(derivations.get(position));
	}

	@Override
	public int getItemCount() {
		return derivations.size();
	}

	static class ViewHolder extends RecyclerView.ViewHolder {

		private TextView tvWord;
		private TextView tvLength;

		public ViewHolder(View itemView) {
			super(itemView);
			tvWord = (TextView) itemView.findViewById(R.id.tv_word);
			tvLength = (TextView) itemView.findViewById(R.id.tv_length);
		}

		public void onBind(final String derivation) {
			tvWord.setText(derivation);
			tvLength.setText(String.valueOf(derivation.length()));
		}
	}
}
