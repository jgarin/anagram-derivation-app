package com.jgarin.anagramderivation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Contains only home fragment. Nothing fancy.
 */
public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
		}
	}
}
