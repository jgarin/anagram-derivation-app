package com.jgarin.anagramderivation;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *
 */

public class FileReaderTask extends AsyncTask<Uri, Void, ArrayList<String>> {

	private final ContentResolver contentResolver;

	/**
	 * Default constructor
	 *
	 * @param contentResolver
	 */
	public FileReaderTask(ContentResolver contentResolver) {
		if (contentResolver == null) {
			throw new IllegalArgumentException("contentResolver cannot be null");
		}
		this.contentResolver = contentResolver;
	}

	/**
	 * Hidden from child classes
	 *
	 * @param params Accepts only 1 Uri for now
	 * @return
	 */
	@Override
	protected final ArrayList<String> doInBackground(Uri... params) {

		if (params.length <= 0) {
			return null;
		}

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(contentResolver.openInputStream(params[0])));
			ArrayList<String> lines = new ArrayList<>();
			String fileContents;
			while ((fileContents = reader.readLine()) != null) {
				lines.add(fileContents);
			}
			return lines;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (Exception e) {
			}
		}
		return null;
	}

	/**
	 * Override this method to get result
	 *
	 * @param lines list of file lines, null if error
	 */
	@Override
	protected void onPostExecute(ArrayList<String> lines) {
	}
}
