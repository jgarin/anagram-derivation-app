package com.jgarin.anagramderivation;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

/**
 * Created by jgarin on 16/01/2017.
 */

public class WordInputFragment extends Fragment implements TextWatcher, View.OnClickListener {

	private static final String TAG = WordInputFragment.class.getSimpleName();

	private TextInputEditText etInputField;
	private View btnOk;
	private ProgressBar progressBar;

	private ArrayList<String> dictionary;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		dictionary = getActivity().getIntent().getStringArrayListExtra(WordInputActivity.DICTIONARY);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_word_input, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		etInputField = (TextInputEditText) view.findViewById(R.id.et_input_field);
		btnOk = view.findViewById(R.id.btn_ok);
		progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

		etInputField.addTextChangedListener(this);
		btnOk.setOnClickListener(this);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// do nothing
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// do nothing
	}

	@Override
	public void afterTextChanged(Editable s) {
		btnOk.setEnabled(s.toString().trim().length() == 3);
	}

	@Override
	public void onClick(View v) {
		final String originalWord = etInputField.getText().toString().trim();
		progressBar.setVisibility(View.VISIBLE);
		new DeriveTask(originalWord, dictionary) {
			@Override
			protected void onPostExecute(ArrayList<String> derivations) {
				progressBar.setVisibility(View.GONE);
				ResultActivity.launch(getActivity(), originalWord, derivations);
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

	}

}
