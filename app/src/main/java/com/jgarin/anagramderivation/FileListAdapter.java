package com.jgarin.anagramderivation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jgarin on 16/01/2017.
 */
public class FileListAdapter extends RecyclerView.Adapter<FileListAdapter.ViewHolder> {

	private final OnItemClickListener onItemClickListener; // we don't need any customizability here
	private List<File> files = new ArrayList<>();

	public FileListAdapter(final OnItemClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}

	public void setItems(final List<File> files) {
		this.files = files;
		notifyDataSetChanged();
	}

	public interface OnItemClickListener {
		void onItemClicked(final View viaw, final ViewHolder viewHolder, final File file);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.file_list_item, parent, false));
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, final int position) {
		final File file = files.get(position);
		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (onItemClickListener != null) {
					onItemClickListener.onItemClicked(v, holder, file);
				}
			}
		});
		holder.onBind(file);
	}

	@Override
	public int getItemCount() {
		return files.size();
	}

	static class ViewHolder extends RecyclerView.ViewHolder {

		private TextView tvFileName;

		public ViewHolder(final View itemView) {
			super(itemView);
			tvFileName = (TextView) itemView.findViewById(R.id.tv_file_name);
		}

		public void onBind(final File file) {
			tvFileName.setText(file.getName());
		}
	}
}
