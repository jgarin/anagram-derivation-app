package com.jgarin.anagramderivation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

public class WordInputActivity extends AppCompatActivity {

	public static final String DICTIONARY = "dictionary";

	/**
	 * The easy way to provide everything the activity needs for normal workflow
	 *
	 * @param context used to start activity
	 * @param words
	 */
	public static void launch(final Context context, final ArrayList<String> words) {
		Intent intent = new Intent(context, WordInputActivity.class);
		intent.putStringArrayListExtra(DICTIONARY, words);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (!getIntent().hasExtra(DICTIONARY)) {
			throw new IllegalArgumentException("You must provide the list of words to use this activity.");
		}

		setContentView(R.layout.activity_word_input);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().replace(R.id.fragment_container, new WordInputFragment()).commit();
		}
	}
}
