package com.jgarin.anagramderivation;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jgarin on 16/01/2017.
 */

public class FilePickerFragment extends Fragment implements FileListAdapter.OnItemClickListener {

	private static final int REQUEST_CODE = 123;
	private RecyclerView recyclerView;
	private ProgressBar progressBar;
	private View emptyMessage;
	private FileListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_file_picker, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
		progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
		emptyMessage = view.findViewById(R.id.empty_message);

		setupList();
		checkStoragePermission();

	}

	private void setupList() {
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
		recyclerView.setAdapter(adapter = new FileListAdapter(this));
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		if (requestCode == REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			readFileList();
		} else {
			getActivity().setResult(Activity.RESULT_CANCELED);
			getActivity().finish();
		}
	}

	private void checkStoragePermission() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
				readFileList();
			} else {
				if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
					new AlertDialog.Builder(getActivity())
							.setMessage(R.string.permission_rationale)
							.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									requestStoragePermission();
								}
							})
							.show();
				} else {
					requestStoragePermission();
				}
			}
		} else {
			readFileList();
		}
	}

	private void requestStoragePermission() {
		ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
	}

	/**
	 * reading list of file in background for better responsiveness
	 */
	private void readFileList() {
		new FileListGetterTask() {
			@Override
			protected void onPostExecute(List<File> files) {
				progressBar.setVisibility(View.GONE);
				recyclerView.setVisibility(files.isEmpty() ? View.GONE : View.VISIBLE);
				emptyMessage.setVisibility(files.isEmpty() ? View.VISIBLE : View.GONE);
				adapter.setItems(files);
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	@Override
	public void onItemClicked(View viaw, FileListAdapter.ViewHolder viewHolder, File file) {
		Uri selectedFileUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", file);
		Intent intent = new Intent();
		intent.setData(selectedFileUri);
		getActivity().setResult(Activity.RESULT_OK, intent);
		getActivity().finish();
	}

	private static class FileListGetterTask extends AsyncTask<Void, Void, List<File>> {

		@Override
		protected List<File> doInBackground(Void... params) {
			File dir = Environment.getExternalStorageDirectory();
			File[] files = dir.listFiles(new FileFilter() { // getting all "txt" files from the dir
				@Override
				public boolean accept(File pathname) {
					return pathname.getName().toLowerCase().endsWith("txt");
				}
			});
			return Arrays.asList(files);
		}
	}
}
