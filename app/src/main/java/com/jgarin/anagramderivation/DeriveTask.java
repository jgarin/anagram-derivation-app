package com.jgarin.anagramderivation;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Performs all the calculations in background thread.
 * All in one thread. Not super fast, but readable and reliable.
 * Passing parameters to 'execute' is not required.
 */
public class DeriveTask extends AsyncTask<Void, Void, ArrayList<String>> {

	private final String originalWord;
	private List<String> dictionary;

	/**
	 * Default constructor.
	 *
	 * @param originalWord
	 * @param dictionary
	 */
	public DeriveTask(String originalWord, List<String> dictionary) {
		this.originalWord = originalWord;
		this.dictionary = dictionary;
	}

	@Override
	protected final ArrayList<String> doInBackground(Void... params) {
		ArrayList<String> result = new ArrayList<>();
		result.add(originalWord); //  at first the original word is the longest anagram derivation
		ArrayList<String> derivations;
		do {
			derivations = new ArrayList<>();
			for (int i = 0; i < result.size(); i++) { // check derivations for every word
				String wordToCheck = result.get(i);
				for (int j = dictionary.size() - 1; j >= 0; j--) { // reverse order so we can clean dictionary inside this cycle
					String dictionaryWord = dictionary.get(j);
					if (dictionaryWord.length() <= wordToCheck.length()) {
						dictionary.remove(j); // shrink dictionary to speed up further iterations
					} else if (dictionaryWord.length() == wordToCheck.length() + 1) {
						if (isAnagramDerivation(dictionaryWord, wordToCheck)
								&& !derivations.contains(dictionaryWord)) {
							derivations.add(dictionaryWord);
						}
					}
				}
			}
			if (derivations.isEmpty()) {
				break;
			} else {
				result = derivations;
			}
		} while (true);
		return result;
	}

	/**
	 * Checks if one word contains all the letters from another word
	 *
	 * @param supposedlyDerivedWord
	 * @param baseWord
	 * @return
	 */
	private final boolean isAnagramDerivation(final String supposedlyDerivedWord, final String baseWord) {
		String tmpWord = supposedlyDerivedWord;
		for (int i = 0; i < baseWord.length(); i++) {
			String character = String.valueOf(baseWord.charAt(i));
			if (tmpWord.contains(character)) {
				tmpWord = tmpWord.replaceFirst(character, ""); // this guarantees that we check for duplicate chars as well
			} else {
				return false;
			}
		}
		return true;
	}
}
